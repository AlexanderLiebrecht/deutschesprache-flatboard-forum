# DeutscheSprache-Flatboard-Forum
Repo mit der deutschen Sprachdatei für das OpenSource Forum namens Flatboard von http://flatboard.free.fr

Ich habe dieses Repos aus Github improtiert und daher ein paar neue Details. In bitbucket.org mache ich auch etwas, auch wenn es hier im Editor keinen Browser 
Spell Check gibt, muss ich versuchen, so zu arbeiten. Dieses Repo kannst du abspalten und die deutsche Sprachdatei gerne installieren.

## Sprachdatei für OpenSource Forum Flatboard

In diesem Repository findest du die deutsche Sprachdatei für die kommende Flatboard-Forum-Version 1.0.2, die derzeit im englischen Support-Forum von Flatboard - oben verlinkt - bereits aktiv ist. Das neue Flatboard-Forum wird demnach in mehreren Sprachen ausgeliefert und ich habe in Absprache mit dem Entwickler Fred aus Frankreich es so festgemacht, gerne für die Verbreitung des mehrsprachigen Flatboard-Forums zu sorgen.

Mir liegt einfach etwas daran und ich würde gerne weitere deutsche Nutzer, nicht nur den eigentlichen Übersetzer, begrüssen. Zur Zeit ist Flatboard im DACH-Raum eher unbekannt, aber das wird schon noch mit der Zeit. Es muss sich mehr herumsprechen und du als German User kannst dazu etwas beitragen. Ich habe bereits zahlreich über das Flatboard-Forum im grossen CMS-Portfolio berichtet und seit heute biete ich diese Sprachdatei auch auf SourceForge.net an, um noch mehr Verbreitung zu erlangen.

Das wird uns dabei helfen, Flatboard so richtig unter die deutschen Nutzer zu bringen.

## Installation der deutschen Sprachdatei

Du lädst die eine Datei hier herunter, entpackst es lokal und lädst alles ins Verzeichnis `/lang` hoch. Danach aktivierst du die deutsche Sprache im Flatboard-Backend unter `config`. Das wäre das, was gemacht werden muss.

Das Resultat meiner kurzen Bemühung bei dem Sprach-File kannst du dir unter https://flatboard.wpzweinull.ch dort im Forum-Frontend mal ansehen.

## Forken dieses Repositorys

Es ist erwünscht sowie erlaubt. Tue etwas für mehr Flatboard-Verbreitung, sowohl auf Github als auch im deutsch-sprachigen Nutzer-Raum. Gemeinsam stossen wir es an und machen das Flatboard-Forum allen deutschen Nutzern etwas schmackhafter. 

> Der Dank geht auch an den Übersetzer Michael aus dem englischen Support-Forum :smile:

Rostock, den 19.08.2017
